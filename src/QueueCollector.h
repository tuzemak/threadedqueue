#ifndef QUEUE_COLLECTOR_H
#define QUEUE_COLLECTOR_H

#include <condition_variable>
#include <mutex>
#include <queue>
#include <thread>
#include <functional>
#include <optional>

template <typename T>
class CQueueCollector {
    using WorkerType = std::function<void(T& arg)>;
    using PredicateType = std::function<bool()>;
    public:
    CQueueCollector() = delete;
    CQueueCollector(size_t threadCapacity, WorkerType worker, PredicateType predicate) :
    m_worker(worker),
    m_predicate(predicate) {

        if (!threadCapacity) return;

        while (threadCapacity--) {
            m_threads.emplace_back(std::thread([this] {
                while (m_predicate()) {
                    std::optional<T> arg = std::nullopt;
                    {
                        std::unique_lock<std::mutex> lock(m_queueMutex);
                        m_queueCondition.wait(lock, [this] { return !m_queue.empty() || !m_predicate(); });
                        if (!m_queue.empty() && m_predicate()) {
                                arg = std::optional<T>(std::move(m_queue.front()));
                                m_queue.pop();
                        }
                    }
                    if (arg) {
                        m_worker(*arg);
                    }
                }
            }));
        }
    };

    void Push(T& arg) {
        std::lock_guard<std::mutex> lock(m_queueMutex);
        m_queue.push(arg);
        m_queueCondition.notify_one();
    }

    ~CQueueCollector() {
        m_predicate = []() -> bool {return false;};
        m_queueCondition.notify_all();
        for (auto& thread : m_threads) {
            if (thread.joinable()) {
                thread.join();
            }
        }   
    }

private:
    std::vector<std::thread> m_threads;
    std::queue<T> m_queue;
    std::mutex m_queueMutex;
    std::condition_variable m_queueCondition;
    WorkerType m_worker;
    PredicateType m_predicate;
};
#endif //OUTER_h