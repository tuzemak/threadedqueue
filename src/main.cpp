#include <iostream>
#include <atomic>
#include "QueueCollector.h"
#include "Outer.h"

int main(int argc, char **argv) {

    std::atomic<bool> terminate(false);

    CQueueCollector<Request*> queue(NumberOfThreads, [&](Request* arg) {
        ProcessRequest(arg);
        delete arg;
    }, [&]() -> bool {return !terminate.load();});

    while (auto request = GetRequest()) {
        queue.Push(request);
    }

    char ch;
    std::cin >> ch;
    //terminate.store(true);
    return 0;
}
