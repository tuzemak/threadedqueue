#include <iostream>
#include <string>
#include <sstream>
#include <random>
#include <thread>
#include <mutex>

#include "Outer.h"

 Request::operator std::string() {
        std::stringstream ss;
        ss << "memory address(" << this << ")";
        return ss.str();
};

Request* GetRequest() throw() {
    static size_t number = 64;
    while(--number) {
        return new Request();
    }

    return nullptr;
};

void ProcessRequest(Request* request) throw() {
    auto static const seed = 123;
    static std::mt19937 urbg {seed};  
    static std::uniform_int_distribution<int> distr {1, 5000};
    auto const millisec = distr(urbg);
    static std::mutex mutex;
    
    if (request) {
        std::lock_guard lock(mutex);
        std::cout << "Processed request:" << std::string(*request) << " sleeps " << millisec << "sec" << std::endl;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(millisec));
};