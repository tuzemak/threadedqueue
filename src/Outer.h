
#ifndef OUTER_H
#define OUTER_H

class Request {
public:
    operator std::string();
};

const int NumberOfThreads = 16;
Request* GetRequest() throw();
void ProcessRequest(Request* request) throw();
#endif //OUTER_h